import CardContainer from './components/CardContainer';

function App() {
	return (
		<main>
			<CardContainer />
		</main>
	);
}

export default App;
