export const data = [
	{
		id: 1,
		name: 'user 1',
		range_staff: 10,
	},
	{
		id: 2,
		name: 'user 2',
		range_staff: 20,
	},
	{
		id: 3,
		name: 'user 3',
		range_staff: 30,
	},
	{
		id: 4,
		name: 'user 4',
		range_staff: 40,
	},
	{
		id: 5,
		name: 'user 5',
		range_staff: 50,
	},
	{
		id: 6,
		name: 'user 6',
		range_staff: 60,
	},
	{
		id: 7,
		name: 'user 7',
		range_staff: 70,
	},
	{
		id: 8,
		name: 'user 8',
		range_staff: 80,
	},
];
