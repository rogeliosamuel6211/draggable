import React, { useState } from 'react';
import '../assets/styles/card.css';

const Card = ({ name, range_staff }) => {
	const [range, setRange] = useState(range_staff);
	console.log(range_staff);
	return (
		<article className="Card">
			<h1>{name}</h1>
			<p>{range}</p>
		</article>
	);
};

export default Card;
