import React from 'react';
import '../assets/styles/cardContainer.css';

import Card from './Card';
import { data } from '../assets/data';

const CardContainer = () => {
	return (
		<>
			<h1>Card container</h1>
			<div className="CardContainer" id="card-container">
				{data.map((persona) => {
					return (
						<Card
							key={persona.id}
							name={persona.name}
							range_staff={persona.range_staff}
						/>
					);
				})}
			</div>
		</>
	);
};

export default CardContainer;
